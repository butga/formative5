import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFileTest {
    public static void main(String[] args) throws FileNotFoundException {
        printFile();
    }

    public static void printFile() throws FileNotFoundException {

        File directory = new File("form1");

        File[] files = directory.listFiles();

        for (File file : files) {
            System.out.println(file.getName());
            getTextFile(file.getName());
        }
    }

    public static void getTextFile(String fileNameParam) throws FileNotFoundException {

        File isiFile = new File("form1/" + fileNameParam);
        Scanner scanner = new Scanner(isiFile);

        System.out.println(scanner.nextLine());
        System.out.println("");
    }
}
